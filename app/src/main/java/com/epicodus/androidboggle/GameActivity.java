package com.epicodus.androidboggle;

import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;

public class GameActivity extends AppCompatActivity {
    @Bind(R.id.letter00TextView) TextView mLetter00TextView;
    @Bind(R.id.letter10TextView) TextView mLetter10TextView;
    @Bind(R.id.letter20TextView) TextView mLetter20TextView;
    @Bind(R.id.letter30TextView) TextView mLetter30TextView;
    @Bind(R.id.letter01TextView) TextView mLetter01TextView;
    @Bind(R.id.letter11TextView) TextView mLetter11TextView;
    @Bind(R.id.letter21TextView) TextView mLetter21TextView;
    @Bind(R.id.letter31TextView) TextView mLetter31TextView;
    @Bind(R.id.letter02TextView) TextView mLetter02TextView;
    @Bind(R.id.letter12TextView) TextView mLetter12TextView;
    @Bind(R.id.letter22TextView) TextView mLetter22TextView;
    @Bind(R.id.letter32TextView) TextView mLetter32TextView;
    @Bind(R.id.letter03TextView) TextView mLetter03TextView;
    @Bind(R.id.letter13TextView) TextView mLetter13TextView;
    @Bind(R.id.letter23TextView) TextView mLetter23TextView;
    @Bind(R.id.letter33TextView) TextView mLetter33TextView;

    @Bind(R.id.answerEditText) EditText mAnswerEditText;
    @Bind(R.id.answerListView) ListView mAnswerListView;
    @Bind(R.id.answerTextView) TextView mAnswerTextView;
    @Bind(R.id.scoreTextView) TextView mScoreTextView;
    @Bind(R.id.timerTextView) TextView mTimerTextView;

    private static final String TAG = GameActivity.class.getSimpleName();
    private ArrayList<String> answers = new ArrayList<String>();
    private String[][] board = new String[4][4];
    private String boardString = new String();
    private String[] alphabet = new String[] {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    Random random = new Random();
    private Integer score = 0;

    private void drawBoard() {
        int vowelCount = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                board[i][j] = alphabet[random.nextInt(25)];
                boardString += board[i][j];
                if (board[i][j].equals("A") || board[i][j].equals("E") || board[i][j].equals("I") || board[i][j].equals("O") || board[i][j].equals("U")) {
                    vowelCount++;
                }
            }
        }
        Log.d(TAG, "vowelCount: " + vowelCount);
        if (vowelCount < 2) {
            Log.d(TAG, "Not enough Vowels. Re-rolling board");
            drawBoard();
        }
        mLetter00TextView.setText(board[0][0]);
        mLetter10TextView.setText(board[1][0]);
        mLetter20TextView.setText(board[2][0]);
        mLetter30TextView.setText(board[3][0]);
        mLetter01TextView.setText(board[0][1]);
        mLetter11TextView.setText(board[1][1]);
        mLetter21TextView.setText(board[2][1]);
        mLetter31TextView.setText(board[3][1]);
        mLetter02TextView.setText(board[0][2]);
        mLetter12TextView.setText(board[1][2]);
        mLetter22TextView.setText(board[2][2]);
        mLetter32TextView.setText(board[3][2]);
        mLetter03TextView.setText(board[0][3]);
        mLetter13TextView.setText(board[1][3]);
        mLetter23TextView.setText(board[2][3]);
        mLetter33TextView.setText(board[3][3]);
        Log.d(TAG, "Board: " + boardString);
    }

    private boolean checkAnswer(String answer) {
        if (answer.length() >= 3) {
            char[] answerArray = answer.toCharArray();
            Log.d(TAG, "boardString: " + boardString);
            Log.d(TAG, ": " + boardString.indexOf(answerArray[0]));
            for (int i = 0; i < answerArray.length; i++) {
                if (boardString.indexOf(answerArray[i]) == -1) {
                    return false;
                }
            }
            answers.add(answer);
            score++;
            return true;
        } else {
            return false;
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        ButterKnife.bind(this);
        drawBoard();

        new CountDownTimer(180000, 1000) {
            public void onTick(long millisUntilFinished) {
                long minutes = millisUntilFinished / 1000 / 60;
                long seconds = millisUntilFinished / 1000 % 60;
                mTimerTextView.setText("0" + minutes + ":" + seconds);
            }

            public void onFinish() {
                Toast.makeText(GameActivity.this, "Done!", Toast.LENGTH_LONG).show();
            }
        }.start();

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, answers);
        mAnswerListView.setAdapter(adapter);

        mAnswerEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                String answer = mAnswerEditText.getText().toString().toUpperCase(Locale.US);
                mAnswerTextView.setText(answer);
                if (checkAnswer(answer)) {
                    mAnswerTextView.setTextColor(Color.GREEN);
                } else {
                    mAnswerTextView.setTextColor(Color.RED);
                }
                mScoreTextView.setText(String.format("%d", answers.size()));
                mAnswerEditText.setText(null);
                return true;
            }
        });

    }
}
